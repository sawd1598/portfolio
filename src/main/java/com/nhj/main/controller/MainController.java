package com.nhj.main.controller;

import com.nhj.main.service.MainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MainController {

    private final MainService mainService;

    @Autowired
    public MainController (MainService mainService) {
        this.mainService = mainService;
    }


    @RequestMapping(value={"/", "main"})
    public ModelAndView main(ModelAndView model) {

        model.addObject("about", mainService.selectAbout());
        model.addObject("aboutExt", mainService.selectAboutExt());

        model.addObject("project", mainService.selectProject());
        model.addObject("projectDetail", mainService.selectProjectDetail());
        model.addObject("projectSkill", mainService.selectProjectSkill());

        model.addObject("skill", mainService.selectSkill());
        model.addObject("licenses", mainService.selectLicenses());

        model.addObject("experience", mainService.selectExperience());
        model.addObject("experienceDetail", mainService.selectExperienceDetail());

        model.addObject("education", mainService.selectEducation());
        model.addObject("educationDetail", mainService.selectEducationDetail());

        model.setViewName("index");

        return model;
    }


}
