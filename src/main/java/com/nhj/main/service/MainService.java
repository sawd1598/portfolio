package com.nhj.main.service;


import com.nhj.model.*;

import java.util.List;

public interface MainService {

    public AboutModel selectAbout();
    public List<AboutModel> selectAboutExt();

    public List<ProjectModel> selectProject();
    public List<ProjectModel> selectProjectDetail();
    public List<ProjectModel> selectProjectSkill();

    public List<SkillModel> selectSkill();
    public List<SkillModel> selectLicenses();

    public List<ExperienceModel> selectExperience();
    public List<ExperienceModel> selectExperienceDetail();

    public List<EducationModel> selectEducation();
    public List<EducationModel> selectEducationDetail();


}


