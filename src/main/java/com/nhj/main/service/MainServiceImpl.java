package com.nhj.main.service;

import com.nhj.main.dao.MainDAO;
import com.nhj.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MainServiceImpl implements MainService{

    private final MainDAO mainDAO;

    @Autowired
    public MainServiceImpl (MainDAO mainDAO) {
        this.mainDAO = mainDAO;
    }

    @Override
    public AboutModel selectAbout() {
        return mainDAO.selectAbout();
    }
    @Override
    public List<AboutModel> selectAboutExt() {
        return mainDAO.selectAboutExt();
    }

    @Override
    public List<ProjectModel> selectProject() {
        return mainDAO.selectProject();
    }
    @Override
    public List<ProjectModel> selectProjectDetail() {
        return mainDAO.selectProjectDetail();
    }
    @Override
    public List<ProjectModel> selectProjectSkill() {
        return mainDAO.selectProjectSkill();
    }

    @Override
    public List<SkillModel> selectSkill() {
        return mainDAO.selectSkill();
    }
    @Override
    public List<SkillModel> selectLicenses() {
        return mainDAO.selectLicenses();
    }

    @Override
    public List<ExperienceModel> selectExperience() {
        return mainDAO.selectExperience();
    }
    @Override
    public List<ExperienceModel> selectExperienceDetail() {
        return mainDAO.selectExperienceDetail();
    }

    @Override
    public List<EducationModel> selectEducation() {
        return mainDAO.selectEducation();
    }
    @Override
    public List<EducationModel> selectEducationDetail() {
        return mainDAO.selectEducationDetail();
    }
}
