package com.nhj.main.dao;

import com.nhj.model.*;
import org.apache.ibatis.session.SqlSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MainDAOImpl implements MainDAO{

    private final SqlSession sqlSession;

    @Autowired
    public MainDAOImpl (SqlSession sqlSession) {
        this.sqlSession = sqlSession;
    }

    @Override
    public AboutModel selectAbout() {
        return sqlSession.selectOne("about.selectAbout");
    }
    @Override
    public List<AboutModel> selectAboutExt() {
        return sqlSession.selectList("about.selectAboutExt");
    }

    @Override
    public List<ProjectModel> selectProject() {
        return sqlSession.selectList("project.selectProject");
    }
    @Override
    public List<ProjectModel> selectProjectDetail() {
        return sqlSession.selectList("project.selectProjectDetail");
    }
    @Override
    public List<ProjectModel> selectProjectSkill() {
        return sqlSession.selectList("project.selectProjectSkill");
    }

    @Override
    public List<SkillModel> selectSkill() {
        return sqlSession.selectList("skill.selectSkill");
    }
    @Override
    public List<SkillModel> selectLicenses() {
        return sqlSession.selectList("skill.selectLicenses");
    }

    @Override
    public List<ExperienceModel> selectExperience() {
        return sqlSession.selectList("experience.selectExperience");
    }
    @Override
    public List<ExperienceModel> selectExperienceDetail() {
        return sqlSession.selectList("experience.selectExperienceDetail");
    }

    @Override
    public List<EducationModel> selectEducation() {
        return sqlSession.selectList("education.selectEducation");
    }
    @Override
    public List<EducationModel> selectEducationDetail() {
        return sqlSession.selectList("education.selectEducationDetail");
    }
}
