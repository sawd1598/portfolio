package com.nhj.board.dao;


import com.nhj.model.BoardModel;

import java.util.HashMap;
import java.util.List;

public interface BoardDAO {


	public List<BoardModel> selectBoardList(HashMap param);

	public int selectBoardCount(HashMap param);

	public BoardModel selectBoardDetail(BoardModel boardModel);

	public int updateBoardHits(BoardModel boardModel) throws Exception;

	public int saveBoardPost(BoardModel boardModel);

	public int updateBoardPost(BoardModel boardModel) throws Exception;

	public int deleteBoardPost(BoardModel boardModel);


}
