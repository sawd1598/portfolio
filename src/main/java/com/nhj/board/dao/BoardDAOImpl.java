package com.nhj.board.dao;


import com.nhj.model.BoardModel;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;



import java.util.HashMap;
import java.util.List;

@Repository
public class BoardDAOImpl implements BoardDAO {


	private final SqlSession sqlSession;

	@Autowired
	public BoardDAOImpl(SqlSession sqlSession) {
		this.sqlSession = sqlSession;
	}

	@Override
	public List<BoardModel> selectBoardList(HashMap param) {
		return sqlSession.selectList("board.selectBoardList", param);
	}

	@Override
	public int selectBoardCount(HashMap param) {
		return sqlSession.selectOne("board.selectBoardCount", param);
	}

	@Override
	public BoardModel selectBoardDetail(BoardModel boardModel) {
		return sqlSession.selectOne("board.selectBoardDetail", boardModel);
	}

	@Override
	public int updateBoardHits(BoardModel boardModel) throws Exception {
		return sqlSession.update("board.updateBoardHits", boardModel);
	}

	public int saveBoardPost(BoardModel boardModel) {
		return sqlSession.insert("board.saveBoardPost", boardModel);
	}

	public int updateBoardPost(BoardModel boardModel) throws Exception {
		return sqlSession.update("board.updateBoardPost", boardModel);
	}

	public int deleteBoardPost(BoardModel boardModel) {
		return sqlSession.delete("board.deleteBoardPost", boardModel);
	}
	
}
