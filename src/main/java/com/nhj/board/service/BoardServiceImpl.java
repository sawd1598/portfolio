package com.nhj.board.service;


import com.nhj.board.dao.BoardDAO;
import com.nhj.model.BoardModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;


@Service
public class BoardServiceImpl implements BoardService {

	private final BoardDAO boardDAO;

	// 생성자 주입
	@Autowired
	public BoardServiceImpl(BoardDAO boardDAO) {
		this.boardDAO = boardDAO;
 	}

	@Override
	public List<BoardModel> selectBoardList(HashMap param) {
		return boardDAO.selectBoardList(param);
	}

	@Override
	public int selectBoardCount(HashMap param) {
		return boardDAO.selectBoardCount(param);
	}

	/*@Transactional(isolation = Isolation.READ_COMMITTED)*/
	@Override
	public BoardModel selectBoardDetail(BoardModel boardModel, String Type) throws Exception{
		if (Type.equals("select")) {
			boardDAO.updateBoardHits(boardModel);
		}
		return boardDAO.selectBoardDetail(boardModel);
	}


	@Override
	public int saveBoardPost(BoardModel boardModel) {
		return boardDAO.saveBoardPost(boardModel);
	}


	/*@Transactional(isolation = Isolation.READ_COMMITTED)*/
	@Override
	public int updateBoardPost(BoardModel boardModel) throws Exception{
		return boardDAO.updateBoardPost(boardModel);
	}

	@Override
	public int deleteBoardPost(BoardModel boardModel) {
		return boardDAO.deleteBoardPost(boardModel);
	}
	
}
