package com.nhj.board.service;

import com.nhj.board.dao.BoardDAO;
import com.nhj.model.BoardModel;

import java.util.HashMap;
import java.util.List;


public interface BoardService {


	public List<BoardModel> selectBoardList(HashMap param);

	public int selectBoardCount(HashMap param);

	/**
	 * 게시글 상세 조회
	 * @param boardModel BoardModel
	 * @return BoardModel
	 */
	public BoardModel selectBoardDetail(BoardModel boardModel, String type) throws Exception;


	public int saveBoardPost(BoardModel boardModel);

	public int updateBoardPost(BoardModel boardModel) throws Exception;

	public int deleteBoardPost(BoardModel boardModel);

}
