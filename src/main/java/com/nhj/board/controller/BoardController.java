package com.nhj.board.controller;

import com.nhj.board.service.BoardService;
import com.nhj.boardApi.service.BoardApiService;
import com.nhj.model.BoardModel;
import com.nhj.model.Criteria;
import com.nhj.model.PageMaker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("/board")
public class BoardController {

	private final BoardService boardService;
	private final BoardApiService boardApiService;

	@Autowired
	public BoardController(BoardService boardService, BoardApiService boardApiService) {
		this.boardService = boardService;
		this.boardApiService = boardApiService;
	}


	/**
	 * 게시판 리스트 조회
	 * @param model
	 * @param cri
	 * @param keyword
	 * @return
	 */
	@GetMapping(value = "/list")
	public ModelAndView boardList(ModelAndView model, Criteria cri, String keyword) {

		PageMaker pageMaker = new PageMaker();

		HashMap<String, Object> param = new HashMap<>();
		param.put("pageStart", cri.getPageStart());
		param.put("pageSize", cri.getPageSize());
		param.put("keyword", keyword);

		pageMaker.setCri(cri);

		List<BoardModel> result = boardService.selectBoardList(param);
		pageMaker.setTotalCount(boardService.selectBoardCount(param));

		model.addObject("boardList", result);
		model.addObject("pageMaker", pageMaker);
		model.addObject("keyword", keyword);

		model.setViewName("/board/list");

		return model;
	}


	/**
	 * 게시판 글쓰기
	 * @param model
	 * @return
	 */
	@GetMapping(value = "/write")
	public ModelAndView boardWrite(ModelAndView model) {

		model.setViewName("/board/write");

		return model;
	}

	/**
	 * 게시판 글 상세보기
	 * @param model
	 * @param boardModel
	 * @param cri
	 * @param keyword
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/detail")
	public ModelAndView boardDetail(ModelAndView model, BoardModel boardModel, Criteria cri, String keyword) throws Exception {

		model.addObject("boardDetail", boardService.selectBoardDetail(boardModel, "select"));
		model.addObject("commentList", boardApiService.getCommentList(boardModel.getBoardNo()));
		model.addObject("commentCount", boardApiService.getCommentCount(boardModel.getBoardNo()));

		PageMaker pageMaker = new PageMaker();
		pageMaker.setCri(cri);
		model.addObject("pageMaker", pageMaker);
		model.addObject("keyword", keyword);

		model.setViewName("/board/detail");

		return model;
	}


	/**
	 * 게시판 작성 글 저장
	 * @param model
	 * @param boardModel
	 * @return
	 */
	@RequestMapping (value = "/save", method = {RequestMethod.POST})
	public ModelAndView boardSave(ModelAndView model, BoardModel boardModel) {

		boardService.saveBoardPost(boardModel);
		model.setViewName("redirect:/board/list");

		return model;
	}

	/**
	 * 게시판 글 수정페이지 이동
	 * @param model
	 * @param boardModel
	 * @param keyword
	 * @param cri
	 * @return
	 * @throws Exception
	 */
	@RequestMapping (value = "/update")
	public ModelAndView boardUpdateSelect(ModelAndView model, BoardModel boardModel, String keyword, Criteria cri) throws Exception {

		model.addObject("boardModel", boardService.selectBoardDetail(boardModel, "update"));
		model.setViewName("/board/update");

		PageMaker pageMaker = new PageMaker();
		pageMaker.setCri(cri);
		model.addObject("page",cri.getPage());
		model.addObject("pageSize",cri.getPageSize());
		model.addObject("pageMaker", pageMaker);
		model.addObject("keyword", keyword);

		return model;
	}

	/**
	 * 게시판 글 수정 후 저장 및 본문 복귀
	 * @param model
	 * @param boardModel
	 * @param cri
	 * @param redAttr
	 * @return
	 * @throws Exception
	 */
	@RequestMapping (value = "/update", method = {RequestMethod.POST})
	public ModelAndView boardUpdate(ModelAndView model, BoardModel boardModel, Criteria cri, RedirectAttributes redAttr) throws Exception{

		boardService.updateBoardPost(boardModel);
		model.setViewName("redirect:/board/detail");

		redAttr.addAttribute("page", cri.getPage());
		redAttr.addAttribute("pageSize", cri.getPageSize());
		redAttr.addAttribute("boardNo", boardModel.getBoardNo());
		redAttr.addAttribute("keyword", boardModel.getKeyword());

		return model;
	}

	/**
	 * 게시판 글 삭제
	 * @param model
	 * @param boardModel
	 * @param keyword
	 * @param cri
	 * @param redAttr
	 * @return
	 */
	@GetMapping (value = "/delete")
	public ModelAndView boardDelete(ModelAndView model, BoardModel boardModel, String keyword, Criteria cri, RedirectAttributes redAttr) {

		PageMaker pageMaker = new PageMaker();
		pageMaker.setCri(cri);

		boardService.deleteBoardPost(boardModel);
		model.setViewName("redirect:/board/list");
		pageMaker.setTotalCount(boardService.selectBoardCount(null));


		if (cri.getPage() > pageMaker.getEndPage()) {
			redAttr.addAttribute("page", pageMaker.getEndPage());
		}
		else {
			redAttr.addAttribute("page", cri.getPage());
		}
		redAttr.addAttribute("pageSize", cri.getPageSize());
		redAttr.addAttribute("keyword", keyword);

		return model;
	}

}
