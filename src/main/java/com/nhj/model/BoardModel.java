package com.nhj.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class BoardModel implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private int boardNo;
	private String title;
	private String content;
	private String hits;
	private String regName;
	private String regDttm;
	private int count;
	private String keyword;
	private int commentCount;

}
