package com.nhj.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class AboutModel implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private int aboutNo;
	private String userName;
	private String userPos;
	private String userIntro;

	private int extNo;
	private String icon;
	private String type;
	private String content;
	private String urlOpen;

}
