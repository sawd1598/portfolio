package com.nhj.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class EducationModel implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private int eduNo;
	private String eduName;
	private String eduSubName;
	private String eduDt;

	private int detailNo;
	private String detailContent;

}
