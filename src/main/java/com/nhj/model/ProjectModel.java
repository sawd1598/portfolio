package com.nhj.model;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
public class ProjectModel implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private int projectNo;
	private String projectName;
	private String projectSubName;
	private String projectDt;
	private String projectRelease;
	private String projectUrl;

	private int detailNo;
	private String detailContent;

	private int skillNo;
	private String skillName;
	private String skillDetail;

}
