package com.nhj.model;

public class Criteria {
    private int page;
    private int pageSize;

    public Criteria() {
        this.page = 1;
        this.pageSize = 10;
    }

    public int getPageStart() {
        return (this.page-1)*pageSize;
    }

    public int getPage() {
        return page;
    }
    public void setPage(int page) {
        if(page <= 0) {
            this.page = 1;
        } else {
            this.page = page;
        }
    }
    public int getPageSize() {
        return pageSize;
    }
    public void setPageSize(int pageSize) {
        if (pageSize <= 0 || pageSize > 100) {
            this.pageSize = 10;
            return;
        }

        this.pageSize = pageSize;
    }
}
