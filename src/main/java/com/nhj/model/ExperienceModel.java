package com.nhj.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ExperienceModel implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private int expNo;
	private String expIntro;
	private String companyName;
	private String companyDept;
	private String companyPos;
	private String empDt;

	private int detailNo;
	private String detailContent;

}
