package com.nhj.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class SkillModel implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private int skillNo;
	private String skillName;

	private int typeNo;
	private String typeName;

	private int licensesNo;
	private String licensesName;
	private String licensesDt;

}
