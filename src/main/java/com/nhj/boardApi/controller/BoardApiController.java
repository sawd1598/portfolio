package com.nhj.boardApi.controller;

import com.nhj.boardApi.service.BoardApiService;
import com.nhj.model.BoardApiModel;
import com.nhj.model.Criteria;
import com.nhj.model.PageMaker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/comments")
public class BoardApiController {

	private final BoardApiService boardApiService;

	@Autowired
	public BoardApiController(BoardApiService boardApiService) {
		this.boardApiService = boardApiService;
	}


	@RequestMapping(value="/{boardNo}", method=RequestMethod.GET)
	public List<BoardApiModel> getCommentList(@PathVariable("boardNo") int boardNo) {
		return boardApiService.getCommentList(boardNo);
	}


	@RequestMapping(value="", method=RequestMethod.POST)
	public void insertComment(@RequestBody HashMap<String, Object> param) {
		boardApiService.insertComment(param);
	}


	@RequestMapping(value="/{boardNo}", method=RequestMethod.DELETE)
	public void deleteComment(@PathVariable("boardNo") int boardNo) {
		boardApiService.deleteComment(boardNo);
	}


}
