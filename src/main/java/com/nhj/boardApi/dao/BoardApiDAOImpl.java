package com.nhj.boardApi.dao;

import com.nhj.model.BoardApiModel;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository
public class BoardApiDAOImpl implements BoardApiDAO {

	private final SqlSession sqlSession;

	@Autowired
	public BoardApiDAOImpl(SqlSession sqlSession) {
		this.sqlSession = sqlSession;
	}

	@Override
	public List<BoardApiModel> getCommentList(int boardNo) {
		return sqlSession.selectList("boardApi.getCommentList", boardNo);
	}

	@Override
	public int getCommentCount(int boardNo) {
		return sqlSession.selectOne("boardApi.getCommentCount", boardNo);
	}

	@Override
	public void insetComment(HashMap<String, Object> param) {
		sqlSession.insert("boardApi.insertComment", param);
	}

	@Override
	public void deleteComment(int boardNo) {
		sqlSession.update("boardApi.deleteComment", boardNo);
	}
}
