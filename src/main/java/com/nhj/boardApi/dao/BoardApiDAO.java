package com.nhj.boardApi.dao;


import com.nhj.model.BoardApiModel;

import java.util.HashMap;
import java.util.List;

public interface BoardApiDAO {


	public List<BoardApiModel> getCommentList(int boardNo);

	public int getCommentCount(int boardNo);

	public void insetComment(HashMap<String, Object> param);

	public void deleteComment(int boardNo);
}
