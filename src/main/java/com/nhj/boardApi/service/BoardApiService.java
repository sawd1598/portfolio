package com.nhj.boardApi.service;

import com.nhj.model.BoardApiModel;
import com.nhj.model.Criteria;
import org.springframework.ui.Model;

import java.util.HashMap;
import java.util.List;


public interface BoardApiService {

	public List<BoardApiModel> getCommentList(int boardNo);

	public int getCommentCount(int boardNo);

	public void insertComment(HashMap<String, Object> param);

	public void deleteComment(int boardNo);
}
