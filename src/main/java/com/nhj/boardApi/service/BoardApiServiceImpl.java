package com.nhj.boardApi.service;


import com.nhj.boardApi.dao.BoardApiDAO;
import com.nhj.model.BoardApiModel;
import com.nhj.model.Criteria;
import com.nhj.model.PageMaker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.HashMap;
import java.util.List;


@Service
public class BoardApiServiceImpl implements BoardApiService {

	private final BoardApiDAO boardApiDAO;

	@Autowired
	public BoardApiServiceImpl(BoardApiDAO boardApiDAO) {
		this.boardApiDAO = boardApiDAO;
	}

	@Override
	public List<BoardApiModel> getCommentList(int boardNo) {
		return boardApiDAO.getCommentList(boardNo);
	}


	@Override
	public int getCommentCount(int boardNo) {
		return boardApiDAO.getCommentCount(boardNo);
	}

	@Override
	public void insertComment(HashMap<String, Object> param) {
		boardApiDAO.insetComment(param);
	}

	@Override
	public void deleteComment(int boardNo) {
		boardApiDAO.deleteComment(boardNo);
	}
}
