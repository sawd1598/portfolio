<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
        <meta name="description" content=""/>
        <meta name="author" content=""/>

        <title>Resume-남혁준</title>

        <link rel="icon" type="image/x-icon" href="resources/assets/img/favicon.ico"/>
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.15.4/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:500,700" rel="stylesheet" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Muli:400,400i,800,800i" rel="stylesheet" type="text/css"/>
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="resources/css/styles.css" rel="stylesheet"/>
        <link href="resources/css/board.css" rel="stylesheet"/>

        <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="resources/js/scripts.js"></script>

        <script>

            $(function readyFn () {
                joinAdmin();
            })

            function openProjectPop(url){
                const popUrl = url;
                const popOption = "top=10, left=10, width=1200, height=750, status=no, menubar=no, toolbar=no, resizable=no, location=no";
                window.open(popUrl, "project", popOption);
            }

            function joinAdmin() {
                $("#inputPassword").keydown(function (e) {
                    if (e.which == 13) {
                        alert("현재 개발 중 또는 개발 예정입니다.");
                    }
                });
            }

        </script>

    </head>

    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
            <a class="navbar-brand js-scroll-trigger" href="#page-top">
                <span class="d-none d-lg-block"><img class="img-fluid img-profile rounded-circle mx-auto mb-2" src="resources/assets/img/profile.jpg" alt="..."/></span>
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                    aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav">
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#about">About</a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#Projects">Projects</a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#skills">Skills</a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#experience">Experience</a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#education">Education</a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#joinAdmin">Admin</a></li>
                </ul>
            </div>
        </nav>
        <!-- Page Content-->
        <div class="container-fluid p-0">
            <!-- About-->
            <section class="resume-section" id="about">
                <div class="resume-section-content">
                    <h1 class="mb-3">
                        <c:out value="${about.userName}" />
                    </h1>
                    <div class="subheading mb-4" style="text-transform:initial; font-size: 30px">
                        <c:out value="${about.userPos}" />
                    </div>
                    <div class="user-intro">
                        <c:out value="${about.userIntro}" />
                    </div>
                    <hr class="my-5">
                    <div class="user-info">
                        <c:forEach items="${aboutExt}" var="aboutExt">
                            <div class="mb-4">
                                <span class="userinfo-icon">
                                    <i class="<c:out value="${aboutExt.icon}" />" color="black"></i>
                                </span>
                                <span>
                                    <c:if test="${aboutExt.urlOpen == 'Y'}">
                                        <span><c:out value="${aboutExt.type}" /> : <a href="<c:out value="${aboutExt.content}" />" target="_blank"><c:out value="${aboutExt.content}" /></a></span>
                                    </c:if>
                                    <c:if test="${aboutExt.urlOpen == 'N'}">
                                        <c:out value="${aboutExt.type}" /> : <c:out value="${aboutExt.content}" />
                                    </c:if>
                                </span>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </section>
            <hr class="m-0"/>

            <!-- Project -->
            <section class="resume-section" id="Projects">
                <div class="resume-section-content">
                    <h2 class="mb-5">Projects</h2>
                    <c:forEach items="${project}" var="project">
                        <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
                            <div class="flex-grow-1">
                                <div class="content-header">
                                    <h3 class="mb-0" style="display: inline"><c:out value="${project.projectName}" /></h3>
                                    <c:if test="${project.projectRelease == 'Y'}">
                                        <button class="project-open-btn btn btn-outline-secondary" onclick="openProjectPop('<c:out value="${project.projectUrl}" />')">프로젝트 확인</button>
                                    </c:if>
                                </div>
                                <div class="subheading mb-3"><c:out value="${project.projectSubName}" /></div>
                                <div class="row">
                                    <!-- 디테일 부분 -->
                                    <div class="col-6">
                                        <ul class="fa-ul mb-0">
                                        <c:forEach items="${projectDetail}" var="projectDetail">
                                            <c:if test="${projectDetail.projectNo == project.projectNo}">
                                                <li class="mb-2">
                                                    <span class="fa-li"><i class="fas fa-circle fa-xs"></i></span>
                                                    <c:out value="${projectDetail.detailContent}" />
                                                </li>
                                            </c:if>
                                        </c:forEach>
                                        </ul>
                                    </div>
                                    <div class="col-6">
                                        <ul class="fa-ul mb-0">
                                            <c:forEach items="${projectSkill}" var="projectSkill">
                                                <c:if test="${projectSkill.projectNo == project.projectNo}">
                                                    <li class="mb-2">
                                                        <span class="fa-li"><i class="fas fa-check"></i></span>
                                                        <c:out value="${projectSkill.skillName}" /> : <c:out value="${projectSkill.skillDetail}" />
                                                    </li>
                                                </c:if>
                                            </c:forEach>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="flex-shrink-0"><span class="text-primary"><c:out value="${project.projectDt}" /></span></div>
                        </div>
                    </c:forEach>
                </div>
            </section>
            <hr class="m-0"/>

            <!-- Skills // 차후 아이콘 DB화 -->
            <section class="resume-section" id="skills">
                <div class="resume-section-content">
                    <h2 class="mb-1">Skills & Licenses</h2>
                    <ul class="list-inline dev-icons mb-4">
                        <li class="list-inline-item"><i class="fab fa-java fa-xs"></i></li>
                        <li class="list-inline-item"><i class="fab fa-js-square fa-xs"></i></li>
                        <li class="list-inline-item"><i class="fab fa-node-js fa-xs"></i></li>
                        <li class="list-inline-item"><i class="fab fa-git-square fa-xs"></i></li>
                        <li class="list-inline-item"><i class="fab fa-gitlab fa-xs"></i></li>
                    </ul>
                    <div class="row">
                        <div class="col-6 subheading mb-3" style="margin-left: 0px">Skills</div>
                        <div class="col-6 subheading mb-3" style="margin-left: 0px">Licenses</div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <ul class="fa-ul mb-0">
                                <c:forEach items="${skill}" var="skill">
                                    <li class="mb-2">
                                        <span class="fa-li"><i class="fas fa-check"></i></span>
                                        <c:out value="${skill.typeName}" /> : <c:out value="${skill.skillName}" />
                                    </li>
                                </c:forEach>
                            </ul>
                        </div>
                        <div class="col-6">
                            <ul class="fa-ul mb-0">
                                <c:forEach items="${licenses}" var="licenses">
                                    <li class="mb-2">
                                        <span class="fa-li"><i class="fas fa-check"></i></span>
                                        <c:out value="${licenses.licensesDt}" /> <c:out value="${licenses.licensesName}" />
                                    </li>
                                </c:forEach>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <hr class="m-0"/>

            <!-- Experience-->
            <section class="resume-section" id="experience">
                <div class="resume-section-content">
                    <h2 class="mb-5">Experience</h2>

                    <c:forEach items="${experience}" var="experience">
                        <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
                            <div class="flex-grow-1">
                                <h3 class="mb-0"><c:out value="${experience.companyName}" /></h3>
                                <div class="subheading mb-3"><c:out value="${experience.companyDept}" /> / <c:out value="${experience.companyPos}" /></div>
                                <ul class="fa-ul mb-0">
                                    <c:forEach items="${experienceDetail}" var="experienceDetail">
                                        <c:if test="${experienceDetail.expNo == experience.expNo}">
                                            <li class="mb-2">
                                                <span class="fa-li"><i class="fas fa-circle fa-xs"></i></span>
                                                <c:out value="${experienceDetail.detailContent}" />
                                            </li>
                                        </c:if>
                                    </c:forEach>
                                </ul>
                                <br>
                                <div style="margin-left: 10px">
                                    <p> <!-- <br>이 아닌 엔터키 적용되게 변경 필요 -->
                                        <c:out value="${experience.expIntro}" />
                                    </p>
                                </div>
                            </div>
                            <div class="flex-shrink-0"><span class="text-primary"><c:out value="${experience.empDt}" /></span></div>
                        </div>
                    </c:forEach>
                </div>
            </section>
            <hr class="m-0"/>

            <!-- Education-->
            <section class="resume-section" id="education">
                <div class="resume-section-content">
                    <h2 class="mb-5">Education</h2>
                    <c:forEach items="${education}" var="education">
                        <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
                            <div class="flex-grow-1">
                                <h3 class="mb-0"><c:out value="${education.eduName}" /></h3>
                                <div class="subheading mb-3"><c:out value="${education.eduSubName}" /></div>
                                <ul class="fa-ul mb-0">
                                    <c:forEach items="${educationDetail}" var="educationDetail">
                                        <c:if test="${educationDetail.eduNo == education.eduNo}">
                                            <li class="mb-2">
                                                <span class="fa-li"><i class="fas fa-circle fa-xs"></i></span>
                                                <c:out value="${educationDetail.detailContent}" />
                                            </li>
                                        </c:if>
                                    </c:forEach>
                                </ul>
                            </div>
                            <div class="flex-shrink-0"><span class="text-primary"><c:out value="${education.eduDt}" /></span></div>
                        </div>
                    </c:forEach>
                </div>
            </section>
            <hr class="m-0"/>

            <!-- Admin Join -->
            <section class="resume-section" id="joinAdmin">
                <div class="resume-section-content">
                    <h2 class="mb-5" style="text-align: center">Admin</h2>
                    <div class="row" style="text-align: center;">
                        <div class="form-group row" style="justify-content: center;">
                            <label for="inputPassword" class="col-sm-1 col-form-label">Password</label>
                            <div class="col-sm-3">
                                <input type="password" class="form-control" id="inputPassword" placeholder="Password">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <hr class="m-0"/>
        </div>
    </body>
</html>
