<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<title>Resume-Board</title>
	<head>
		<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
	</head>

	<script>


		function space_validation() {
			if ($("#title").val().trim() == "") {
				alert("제목을 입력해주세요.");
				$("#title").focus();
				return false;
			}
			if ($("#regName").val().trim() == "") {
				alert("작성자를 입력해주세요.");
				$("#regName").focus();
				return false;
			}
			if ($("#content").val().trim() == "") {
				alert("내용을 입력해주세요.");
				$("#content").focus();
				return false;
			}

			$("#boardForm").submit();
		}
	</script>

	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card card-with-border">
						<div class="card-header">
							<h5>게시판 글쓰기</h5>
						</div>
					</div>
				</div>
				<div class="col-12">
					<div class="card card-with-border">
						<div class="card-body">
							<div class="table-responsive">
								<%--<table class="table text-center">--%>
								<form name="boardForm" id="boardForm" role="form" method="post" action="/board/save">
									<table class="table text-center">
										<tbody>
										<tr>
											<td>제목</td>
										</tr>
										<tr>
											<td>
												<input type="text" class="form-control" id="title" name="title" placeholder="제목을 입력해 주세요">
											</td>
										</tr>
										<tr>
											<td>작성자</td>
										</tr>
										<tr>
											<td>
												<input type="text" class="form-control" id="regName" name="regName" placeholder="작성자를 입력해 주세요">
											</td>
										</tr>
										<tr>
											<td>내용</td>
										</tr>
										<tr>
											<td>
												<textarea class="form-control" rows="5" id="content" name="content" placeholder="내용을 입력해 주세요" style="height: 300px"></textarea>
											</td>
										</tr>
										</tbody>
									</table>
									<div class="col-12">
										<button type="button" class="btn btn-ml btn-primary" id="btnSave" style="margin-left: 10px" onclick="space_validation();">저장</button>
											<%--<button type="submit" class="btn btn-ml btn-primary" id="btnSave" style="margin-left: 10px">저장</button>--%>
										<button type="button" class="btn btn-ml btn-danger" id="btnCancel" onclick="window.history.back();" style="margin-left: 10px">취소</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>