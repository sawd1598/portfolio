<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card card-with-border">
						<div class="card-header">
							<h5>게시판 글 수정</h5>
						</div>
					</div>
				</div>
				<div class="col-12">
					<div class="card card-with-border">
						<div class="card-body">
							<div class="table-responsive">
								<form name="form" id="form" role="form" method="post" action='<c:url value='/board/update'/>'>
									<input type="hidden" name="boardNo" id="boardNo" value="${boardModel.boardNo}">
									<input type="hidden" name="page" id="page" value="${page}">
									<input type="hidden" name="pageSize" id="pageSize" value="${pageSize}">
									<input type="hidden" name="keyword" id="keyword" value="${keyword}">

									<table class="table text-center">
										<tbody>
										<tr>
											<td>제목</td>
										</tr>
										<tr>
											<td>
												<input type="text" class="form-control" name="title" id="title" placeholder="제목을 입력해 주세요" value="${boardModel.title}">
											</td>
										</tr>
										<tr>
											<td>작성자</td>
										</tr>
										<tr>
											<td>
												<input type="text" class="form-control" name="regName" id="regName" placeholder="작성자를 입력해 주세요" value="${boardModel.regName}" >
											</td>
										</tr>
										<tr>
											<td>내용</td>
										</tr>
										<tr>
											<td>
												<textarea class="form-control" rows="5" name="content" id="content" placeholder="내용을 입력해 주세요" style="height: 350px">${boardModel.content}</textarea>
											</td>
										</tr>
										</tbody>
									</table>
									<div class="col-12">
										<button type="submit" class="btn btn-ml btn-primary" id="btnSave" style="margin-left: 10px">저장</button>
										<button type="button" class="btn btn-ml btn-danger" id="btnCancel" onclick="window.history.back();" style="margin-left: 10px">취소</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>