<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <title>Resume-Board</title>
    <head>
        <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="/resources/css/board.css">

        <style type="text/css">
            a { color:black; text-decoration: none;}
            a:visited { text-decoration: none;}
            a:hover { color:black; text-decoration: none;}
            a:active { color:black; text-decoration: none;}
        </style>

        <script type="text/javascript">
            // 게시판 게시글 갯수
            $(function() {
                $('#pageSize').change(function() {
                    location.href = "<c:url value='/board/list?page=1&pageSize='/>" + $("#pageSize").val()
                        <c:if test="${not empty keyword}">
                        + "&keyword=" + "${keyword}"
                    </c:if>
                })
            });

            // 검색 실행
            function searchKeyword() {
                location.href = "<c:url value='/board/list${pageMaker.makeQueryPage(1) }'/>" + "&keyword=" + $("#inputKeyword").val()
            }

        </script>
    </head>

    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-with-border">
                        <div class="card-header">
                            <h5>게시판</h5>
                        </div>
                        <div class="card-body">
                            <div class="row col-12">
                                <div class="col-4">
                                    <input class="form-control" type="text" name="inputKeyword" id="inputKeyword" placeholder="제목 또는 내용 검색"
                                           onkeypress="if( event.keyCode == 13 ){searchKeyword();}"/>
                                </div>
                                <div class="col-auto">
                                    <button type="button" id="btnSearch" data-role="btnSearch" class="btn btn-primary" onclick="searchKeyword();">검색</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12">
                    <div class="card card-with-border">
                        <div class="card-body">
                            <div class="row mb-3">
                                <div class="col-2">
                                    <select class="form-control" name="pageSize" id="pageSize">
                                        <option value="10" <c:if test="${pageMaker.cri.pageSize == 10}">selected</c:if>>10개씩 보기</option>
                                        <option value="20" <c:if test="${pageMaker.cri.pageSize == 20}">selected</c:if>>20개씩 보기</option>
                                        <option value="30" <c:if test="${pageMaker.cri.pageSize == 30}">selected</c:if>>30개씩 보기</option>
                                        <option value="40" <c:if test="${pageMaker.cri.pageSize == 40}">selected</c:if>>40개씩 보기</option>
                                        <option value="50" <c:if test="${pageMaker.cri.pageSize == 50}">selected</c:if>>50개씩 보기</option>
                                    </select>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table text-center">
                                    <thead>
                                        <tr>
                                            <th style="width: 10%">No</th>
                                            <th style="width: 50%">제목</th>
                                            <th style="width: 10%">조회수</th>
                                            <th style="width: 30%">등록일시</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${boardList}" var="boardList">
                                            <tr>
                                                <td><c:out value="${boardList.boardNo}"/></td>
                                                <c:if test="${not empty keyword}">
                                                    <td><a href='<c:url value='/board/detail${pageMaker.makeQueryPage(pageMaker.cri.page, boardList.boardNo, keyword) }'/>'>
                                                        <c:out value="${boardList.title}"/> </a>
                                                        <c:if test="${boardList.commentCount >= 1}">
                                                            <span class="board-comment">[${boardList.commentCount}]</span>
                                                        </c:if>
                                                    </td>
                                                </c:if>
                                                <c:if test="${empty keyword}">
                                                    <td><a href='<c:url value='/board/detail${pageMaker.makeQueryPage(pageMaker.cri.page, boardList.boardNo) }'/>'>
                                                        <c:out value="${boardList.title}"/> </a>
                                                        <c:if test="${boardList.commentCount >= 1}">
                                                            <span class="board-comment">[${boardList.commentCount}]</span>
                                                        </c:if>
                                                    </td>
                                                </c:if>
                                                <td><c:out value="${boardList.hits}"/></td>
                                                <td><c:out value="${boardList.regDttm}"/></td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <nav aria-label="Page navigation example">
                                        <ul class="pagination">
                                            <c:if test="${not empty keyword}">
                                                <c:if test="${pageMaker.prev }">
                                                    <li class="page-item"><a class="page-link"
                                                                             href='<c:url value="/board/list${pageMaker.makeQueryPage(pageMaker.startPage-1, keyword) }"/>'>Previous</a></li>
                                                </c:if>
                                                <c:forEach begin="${pageMaker.startPage }" end="${pageMaker.endPage }" var="pageNum">
                                                    <li class="page-item"><a class="page-link"
                                                                             href='<c:url value="/board/list${pageMaker.makeQueryPage(pageNum, keyword) }"/>'>${pageNum }</a></li>
                                                </c:forEach>
                                                <c:if test="${pageMaker.next && pageMaker.endPage >0 }">
                                                    <li class="page-item"><a class="page-link"
                                                                             href='<c:url value="/board/list${pageMaker.makeQueryPage(pageMaker.endPage+1, keyword) }" />'>Next</a></li>
                                                </c:if>
                                            </c:if>
                                            <c:if test="${empty keyword}">
                                                <c:if test="${pageMaker.prev }">
                                                    <li class="page-item"><a class="page-link" href='<c:url value="/board/list${pageMaker.makeQueryPage(pageMaker.startPage-1) }"/>'>Previous</a></li>
                                                </c:if>
                                                <c:forEach begin="${pageMaker.startPage }" end="${pageMaker.endPage }" var="pageNum">
                                                    <li class="page-item"><a class="page-link" href='<c:url value="/board/list${pageMaker.makeQueryPage(pageNum) }"/>'>${pageNum }</a></li>
                                                </c:forEach>
                                                <c:if test="${pageMaker.next && pageMaker.endPage >0 }">
                                                    <li class="page-item"><a class="page-link" href='<c:url value="/board/list${pageMaker.makeQueryPage(pageMaker.endPage+1) }" />'>Next</a></li>
                                                </c:if>
                                            </c:if>
                                        </ul>
                                    </nav>
                                </div>
                                <div class="col-2">
                                    <a href="/board/write" class="btn btn-primary" style="float:right">글쓰기</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>