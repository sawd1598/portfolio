<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
	<title>Resume-Board</title>
	<head>
		<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
		<script src="https://use.fontawesome.com/releases/v5.15.4/js/all.js" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
		<link rel="stylesheet" href="/resources/css/board.css">

		<script>
			const restIp = "<spring:eval expression="@property['active.ip']"/>";
			const restUrl = ""

			console.log(restIp);


			const boardNo = ${boardDetail.boardNo};

			$(function readyFn () {
				getComment();
			})

			function getComment() {
				$.ajax({
					url: "http://localhost:8080/comments/"+boardNo,
					type: "GET",
					dataType : "json",
					async : false
				})
				.done(function (data, textStatus, jqXHR) {
					if (data.length >= 1) {
						$("#commentCount").text("("+data.length+")");
					}
					else {
						$("#commentCount").text("");
					}
					$("#commentWrite").val("")
					$("#cmtList").children().remove()
					$.each(data, function(idx, item) {
						const commentList =
								"<li class='cmt-row'>" +
								"<div class='cmt-one'>" +
								"<div class='cmt-title'>" +
								"<span>"+item.regDttm+"</span><a class='del-comment' " +
								"href='javascript:deleteComment("+item.commentNo+");' ><i class='fas fa-backspace'></i></a></div>" +
								"<div class='cmt-body'>" +
								/*"<span id='commentMain' class='comment-main'>"+item.content+"</span>" +*/
								"<span id='commentMain' class='comment-main' style='white-space:pre;'>"+item.content+"</span>" +
								"<hr class='comment-hr'>" +
								"</div></div></li>"
						$("#cmtList").append(commentList);
					});
				})
				.fail(function (jqXHR) {
					alert("[ Error Code : " + jqXHR.status + " Error Text : " + jqXHR.statusText + " ] 에러가 발생했습니다. 관리자에게 연락 바랍니다.");
				});
			}


			function insetComment () {
				const content = $("#commentWrite").val().trim();
				if (content.length == 0) {
					alert("댓글을 입력해 주세요!");
					$("#commentWrite").focus();
					return false;
				}
				const comment = {
					boardNo : boardNo,
					content : content
				}
				$.ajax({
					url: "http://localhost:8080/comments",
					type: "POST",
					contentType : "application/json; charset=utf-8",
					data: JSON.stringify (comment),
					async : false
				})
				.done(function () {
					getComment();
				})
				.fail(function (jqXHR) {
					alert("[ Error Code : " + jqXHR.status + " Error Text : " + jqXHR.statusText + " ] 에러가 발생했습니다. 관리자에게 연락 바랍니다.");
				});
			}

			function deleteComment(commentNo) {
				if ( confirm("정말 삭제하시겠습니까?") == true) {
					$.ajax({
						url: "http://localhost:8080/comments/"+commentNo,
						type: "DELETE",
						async : false
					})
					.done(function () {
						getComment();
					})
					.fail(function (jqXHR) {
						alert("[ Error Code : " + jqXHR.status + " Error Text : " + jqXHR.statusText + " ] 에러가 발생했습니다. 관리자에게 연락 바랍니다.");
					});
				}
				else {
					return false;
				}
			}
		</script>

	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card card-with-border">
						<div class="card-header">
							<h5>게시판 상세</h5>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="card card-with-border">
						<div class="card-body">
							<div class="board-wrap">
								<div class="info-wrap">
									<div class="align-self-center col-4" style="text-align: left"><c:out value="${boardDetail.regName}" /></div>
									<div class="align-self-center col-4"><c:out value="${boardDetail.regDttm}" /></div>
									<div class="align-self-center col-4" style="text-align: right">조회수 : <c:out value="${boardDetail.hits}" /></div>
								</div>
								<div class="title-wrap">
									<div class="col-1"></div>
									<div class="col-11"><c:out value="${boardDetail.title}" /></div>
								</div>
								<div class="content-wrap">
									<div class="col-1"></div>
									<div class="col-11" style="white-space:pre;"><c:out value="${boardDetail.content}" /></div>
								</div>
								<div class="edit-wrap">
									<div class="col-12">
										<c:if test="${not empty keyword}">
											<a href='<c:url value='/board/list${pageMaker.makeQueryPage(pageMaker.cri.page, keyword) }'/>' class="btn btn-ml btn-secondary"> 목록</a>
										</c:if>
										<c:if test="${empty keyword}">
											<a href='<c:url value='/board/list${pageMaker.makeQueryPage(pageMaker.cri.page) }'/>' class="btn btn-ml btn-secondary"> 목록</a>
										</c:if>
										<a href='<c:url value='/board/update${pageMaker.makeQueryPage(pageMaker.cri.page, boardDetail.boardNo, keyword) }'/>'
										   class="btn btn-ml btn-primary"> 수정</a>
										<a href='<c:url value='/board/delete${pageMaker.makeQueryPage(pageMaker.cri.page, boardDetail.boardNo, keyword) }'/>'
										   onclick="return confirm('정말 삭제하시겠습니까?');" class="btn btn-ml btn-danger">삭제</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="card card-with-border">
						<div class="card-body">
							<!-- 댓글창 전체 영역 박스 -->
							<div class="cmt-wrap">
								<!-- 댓글창 헤더 영역 / 댓글, 새로고침 등 -->
								<div class="cmt-head">
									<h5>
										<span style="margin-right: 5px"><i class="far fa-comment"></i></span>
										댓글<span id="commentCount"></span>
									</h5>
								</div>
								<!-- 댓글창 본문 영역 박스 -->
								<div class="cmt-body">
									<!-- 댓글창 본문 영역 -->
									<div class="cmt-content">
										<!-- 댓글창 본문 -->
										<ul id="cmtList" class="cmt-list">
											<!-- 댓글창 각 댓글 -->
										</ul>
									</div>
								</div>
								<div class="cmt-space">
									<!-- 빈공간 -->
								</div>
								<!-- 댓글 쓰기 영역 -->
								<div class="cmt-write-wrap">
									<div class="cmt-write" style="width:90%; float:left; margin-left: 15px">
										<textarea id="commentWrite" class="comment-write" style="width: 100%; min-height: 66px;"></textarea>
									</div>
									<div style="float:left; margin-left:20px; width:68px; height:66px">
										<button class="btn btn-outline-dark" style="width:100%; height: 100%;" onclick="insetComment();">등록</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="cmt-space">
			<!-- container-fluid 종료 -->
		</div>

						<%--<div class="table-responsive">
							<table class="table text-center">
								<tbody>
									<colgroup>
										<col style="width: 30%">
										<col style="width: 40%">
										<col style="width: 30%">
									</colgroup>
									&lt;%&ndash;<tr style="background-color: rgba(235,235,235,0.7)">&ndash;%&gt;
									<tr>
										<td><c:out value="${boardDetail.regName}" /></td>
										<td><c:out value="${boardDetail.regDttm}" /></td>
										<td>조회수 : <c:out value="${boardDetail.hits}" /></td>
										&lt;%&ndash;<td style="width: 30%"><c:out value="${boardDetail.regName}" /></td>
										<td style="width: 40%"><c:out value="${boardDetail.regDttm}" /></td>
										<td style="width: 30%">조회수 : <c:out value="${boardDetail.hits}" /></td>&ndash;%&gt;
										&lt;%&ndash;<td style="width: 35%">제목</td>
										<td colspan="3">${boardDetail.title}</td>&ndash;%&gt;
									</tr>
									<tr>
										<td></td>
										<td colspan="2" style="text-align: left;"><c:out value="${boardDetail.title}" /></td>
									</tr>
									<tr class="tr-content">
										<td></td>
										<td colspan="2" style="text-align: left"><c:out value="${boardDetail.content}" /></td>
									</tr>
								</tbody>
							</table>
						</div>--%>

	</body>
</html>
