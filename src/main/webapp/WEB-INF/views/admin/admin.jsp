<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
        <meta name="description" content=""/>
        <meta name="author" content=""/>

        <title>Resume-남혁준</title>

        <link rel="icon" type="image/x-icon" href="resources/assets/img/favicon.ico"/>
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.15.4/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:500,700" rel="stylesheet" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Muli:400,400i,800,800i" rel="stylesheet" type="text/css"/>
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="resources/css/styles.css" rel="stylesheet"/>
        <link href="resources/css/board.css" rel="stylesheet"/>

        <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="resources/js/scripts.js"></script>

        <script>
            function openBoardPop(){
                const popUrl = "/board/list";
                const popOption = "top=10, left=10, width=1200, height=750, status=no, menubar=no, toolbar=no, resizable=no, location=no";
                window.open(popUrl, "board", popOption);
            }

            function joinAdmin() {
                alert("현재 개발 중 또는 개발 예정입니다.");
            }

            function test() {

            }
        </script>

    </head>

    <body id="page-top">
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">
            <span class="d-block d-lg-none">Clarence Taylor</span>
            <span class="d-none d-lg-block"><img class="img-fluid img-profile rounded-circle mx-auto mb-2" src="resources/assets/img/profile.jpg" alt="..."/></span>
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav">
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#about">About</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#Projects">Projects</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#skills">Skills</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#experience">Experience</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#education">Education</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#joinAdmin">Admin</a></li>
            </ul>
        </div>
    </nav>
    <!-- Page Content-->
    <div class="container-fluid p-0">
        <!-- About-->
        <section class="resume-section" id="about">
            <div class="resume-section-content">
                <h1 class="mb-3">
                    남 혁 준 (ADMIN)
                </h1>
                <div class="subheading mb-5" style="text-transform:initial; font-size: 30px">
                    Junior Web Developer
                </div>
                <p class="lead mb-4">
                    안녕하세요, Java Back-end 개발자의 꿈을 가진 남혁준 입니다.
                </p>
                <p class="lead mb-4">
                    오랫동안 컴퓨터 혹은 개발과 관련된 길만 걸어오며, 근래 잠시 다른 내일을 찾아 쉬는 기간이 있었습니다.<br>
                    잠시 내려놓은 기간이었지만, 코딩과 관련된 질문을 받아 같이 문제를 풀 때면 이 길을 놓았다는게 아쉽게 느껴지곤 했습니다.
                </p>
                <p class="lead mb-4">
                    긴 시간 끝에 포기할 수 없다고 생각했습니다.<br>
                    쉬었던 기간이 짧지 않은 만큼, 지금 이 포트폴리오 페이지를 개발하며 다시 한번 능력을 정비하고 보여드리고자 합니다. </p>
                </p>
                <hr class="my-5">
                <div class="user-info">
                    <div class="mb-4">
                        <span class="userinfo-icon"><i class="fab fa-git-square fa-lg" color="black"></i></span>
                        <span>GitLab : <a href="https://gitlab.com/sawd1598" target="_blank">https://gitlab.com/sawd1598</a></span>
                    </div>
                    <div class="mb-4">
                        <span class="userinfo-icon"><i class="fas fa-envelope-square fa-lg" color="black"></i></span>
                        <span>E-Mail : sawd1598@naver.com</span>
                    </div>
                    <div class="mb-4">
                        <span class="userinfo-icon"><i class="fas fa-mobile-alt fa-lg" color="black"></i></span>
                        <span>H.P : 010-4582-8903</span>
                    </div>
                </div>
            </div>
        </section>
        <hr class="m-0"/>
        <!-- Project -->
        <section class="resume-section" id="Projects">
            <div class="resume-section-content">
                <h2 class="mb-5">Projects</h2>
                <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
                    <div class="flex-grow-1">
                        <div class="content-header">
                            <h3 class="mb-0" style="display: inline">게시판 방명록</h3> <h5 style="display: inline; color:#6c757d">(토이 프로젝트)</h5>
                            <button class="btn btn-outline-secondary" onclick="openBoardPop()" style="display: inline; vertical-align: top; margin-top: 2px; margin-left: 30px;">프로젝트 확인</button>
                        </div>
                        <div class="subheading mb-3">Toy Project</div>
                        <div class="row">
                            <div class="col-6">
                                <ul class="fa-ul mb-0">
                                    <li class="mb-2">
                                        <span class="fa-li"><i class="fas fa-circle fa-xs"></i></span>
                                        Spring MVC + Maven Project
                                    </li>
                                    <li class="mb-2">
                                        <span class="fa-li"><i class="fas fa-circle fa-xs"></i></span>
                                        CRUD 및 SQL, properties 등 여러 기본기 숙련
                                    </li>
                                    <li class="mb-2">
                                        <span class="fa-li"><i class="fas fa-circle fa-xs"></i></span>
                                        Restful 방식 및 RestAPI 구현 테스트 및 적용
                                    </li>
                                </ul>
                            </div>
                            <div class="col-6">
                                <ul class="fa-ul mb-0">
                                    <li class="mb-2">
                                        <span class="fa-li"><i class="fas fa-check"></i></span>
                                        Languages : Java, JavaScript, jQuery, JSP
                                    </li>
                                    <li class="mb-2">
                                        <span class="fa-li"><i class="fas fa-check"></i></span>
                                        DataBase : Maria DB
                                    </li>
                                    <li class="mb-2">
                                        <span class="fa-li"><i class="fas fa-check"></i></span>
                                        Server & Deploy OS : Tomcat, Windows
                                    </li>
                                    <li class="mb-2">
                                        <span class="fa-li"><i class="fas fa-check"></i></span>
                                        Tool & Devops : Intellij, BootStrap
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="flex-shrink-0"><span class="text-primary">2022. 02. 01. - 진행중</span></div>
                </div>
                <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
                    <div class="flex-grow-1">
                        <h3 class="mb-0">소형 생산관리 시스템 (MES Lite)</h3>
                        <div class="subheading mb-3">Link2US</div>
                        <div class="row">
                            <div class="col-6">
                                <ul class="fa-ul mb-0">
                                    <li class="mb-2">
                                        <span class="fa-li"><i class="fas fa-circle fa-xs"></i></span>
                                        Spring MVC + Maven, AXBOOT, Kendo UI를 핵심으로 구현한 MES 소형화 버전
                                    </li>
                                    <li class="mb-2">
                                        <span class="fa-li"><i class="fas fa-circle fa-xs"></i></span>
                                        물류 및 고객사 정보 등 기준 정보 관리 시스템 개발
                                    </li>
                                    <li class="mb-2">
                                        <span class="fa-li"><i class="fas fa-circle fa-xs"></i></span>
                                        자재 입·출고, 제품 생산 및 발주 전산 관리 시스템 개발
                                    </li>
                                    <li class="mb-2">
                                        <span class="fa-li"><i class="fas fa-circle fa-xs"></i></span>
                                        생산 현황 그래프 시각화 구현
                                    </li>
                                </ul>
                            </div>
                            <div class="col-6">
                                <ul class="fa-ul mb-0">
                                    <li class="mb-2">
                                        <span class="fa-li"><i class="fas fa-check"></i></span>
                                        Languages : Java, JavaScript, jQuery, JSP
                                    </li>
                                    <li class="mb-2">
                                        <span class="fa-li"><i class="fas fa-check"></i></span>
                                        DataBase : Maria DB
                                    </li>
                                    <li class="mb-2">
                                        <span class="fa-li"><i class="fas fa-check"></i></span>
                                        Server & Deploy OS : Tomcat, Windows, Linux
                                    </li>
                                    <li class="mb-2">
                                        <span class="fa-li"><i class="fas fa-check"></i></span>
                                        Tool & Devops : Intellij, AXBOOT, Kendo UI, BootStrap, Zebra Printer, Jasper Studio

                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>
                    <div class="flex-shrink-0"><span class="text-primary">2019. 12. 01. - 2019. 04. 30.</span></div>
                </div>
                <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
                    <div class="flex-grow-1">
                        <h3 class="mb-0">웹 쇼핑몰 팀 프로젝트</h3>
                        <div class="subheading mb-3">한국정보기술연구원</div>
                        <div class="row">
                            <div class="col-6">
                                <ul class="fa-ul mb-0">
                                    <li class="mb-2">
                                        <span class="fa-li"><i class="fas fa-circle fa-xs"></i></span>
                                        MVC (JSP Model 2) 패턴으로 구현한 쇼핑몰
                                    </li>
                                    <li class="mb-2">
                                        <span class="fa-li"><i class="fas fa-circle fa-xs"></i></span>
                                        로그인, 가입 및 수정, 상품 조회 및 등록, 장바구니 시스템 등 쇼핑몰 핵심 기능 구현
                                    </li>
                                    <li class="mb-2">
                                        <span class="fa-li"><i class="fas fa-circle fa-xs"></i></span>
                                        Amazon Web Service(AWS), Amazon RDS(Oracle)를 활용하여 라이브 환경 배포 및 접속 확인
                                    </li>
                                </ul>
                            </div>
                            <div class="col-6">
                                <ul class="fa-ul mb-0">
                                    <li class="mb-2">
                                        <span class="fa-li"><i class="fas fa-check"></i></span>
                                        Languages : Java, JavaScript, jQuery, JSP
                                    </li>
                                    <li class="mb-2">
                                        <span class="fa-li"><i class="fas fa-check"></i></span>
                                        DataBase : Oracle
                                    </li>
                                    <li class="mb-2">
                                        <span class="fa-li"><i class="fas fa-check"></i></span>
                                        Server & Deploy OS : Tomcat, AWS, Windows, Linux
                                    </li>
                                    <li class="mb-2">
                                        <span class="fa-li"><i class="fas fa-check"></i></span>
                                        Tool & Devops : Eclipse
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="flex-shrink-0"><span class="text-primary">2018. 08. 10. - 2018. 08. 24.</span></div>
                </div>
                <div class="d-flex flex-column flex-md-row justify-content-between">
                    <div class="flex-grow-1">
                        <h3 class="mb-0">나만의 홈페이지 제작 개인 프로젝트</h3>
                        <div class="subheading mb-3">동양미래대학교</div>
                        <div class="row">
                            <div class="col-6">
                                <ul class="fa-ul mb-0">
                                    <li class="mb-2">
                                        <span class="fa-li"><i class="fas fa-circle fa-xs"></i></span>
                                        JSP (JSP model 1) 를 중심으로 구현한 홈페이지
                                    </li>
                                    <li class="mb-2">
                                        <span class="fa-li"><i class="fas fa-circle fa-xs"></i></span>
                                        Database, Session, FTP 업로드 등 기본기 숙련 및 활용
                                    </li>
                                    <li class="mb-2">
                                        <span class="fa-li"><i class="fas fa-circle fa-xs"></i></span>
                                        로그인, 회원가입, 게시판, 이미지 업로드 등 홈페이지 기본 구현
                                    </li>
                                </ul>
                            </div>
                            <div class="col-6">
                                <ul class="fa-ul mb-0">
                                    <li class="mb-2">
                                        <span class="fa-li"><i class="fas fa-check"></i></span>
                                        Languages : Java, JavaScript, jQuery, JSP
                                    </li>
                                    <li class="mb-2">
                                        <span class="fa-li"><i class="fas fa-check"></i></span>
                                        DataBase : MySQL
                                    </li>
                                    <li class="mb-2">
                                        <span class="fa-li"><i class="fas fa-check"></i></span>
                                        Server & Deploy OS : Tomcat, Windows
                                    </li>
                                    <li class="mb-2">
                                        <span class="fa-li"><i class="fas fa-check"></i></span>
                                        Tool & Devops : Eclipse
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="flex-shrink-0"><span class="text-primary">2017. 09. 01. - 2017. 10. 31.</span></div>
                </div>
            </div>
        </section>
        <hr class="m-0"/>
        <!-- Skills-->
        <section class="resume-section" id="skills">
            <div class="resume-section-content">
                <h2 class="mb-1">Skills & Licenses</h2>
                <ul class="list-inline dev-icons mb-4">
                    <li class="list-inline-item"><i class="fab fa-java fa-xs"></i></li>
                    <li class="list-inline-item"><i class="fab fa-js-square fa-xs"></i></li>
                    <li class="list-inline-item"><i class="fab fa-node-js fa-xs"></i></li>
                    <li class="list-inline-item"><i class="fab fa-git-square fa-xs"></i></li>
                    <li class="list-inline-item"><i class="fab fa-gitlab fa-xs"></i></li>
                </ul>
                <div class="row">
                    <div class="col-6 subheading mb-3" style="margin-left: 0px">Skills</div>
                    <div class="col-6 subheading mb-3" style="margin-left: 0px">Licenses</div>
                </div>
                <div class="row">
                    <!-- 좌 -->
                    <div class="col-6">
                        <ul class="fa-ul mb-0">
                            <li class="mb-2">
                                <span class="fa-li"><i class="fas fa-check"></i></span>
                                Languages : Java, Java Script, jQuery, JSP, C, Android
                            </li>
                            <li class="mb-2">
                                <span class="fa-li"><i class="fas fa-check"></i></span>
                                Tool : Intellij, Eclipse
                            </li>
                            <li class="mb-2">
                                <span class="fa-li"><i class="fas fa-check"></i></span>
                                DataBase : Maria DB, MySQL, Oracle
                            </li>
                            <li class="mb-2">
                                <span class="fa-li"><i class="fas fa-check"></i></span>
                                Server & OS : Tomcat, Windows Server, Linux Server
                            </li>
                            <li class="mb-2">
                                <span class="fa-li"><i class="fas fa-check"></i></span>
                                Devops : GCP, AWS, Kendo UI, Zebra Printer, Jasper Studio
                            </li>
                        </ul>
                    </div>
                    <!-- 우 -->
                    <div class="col-6">
                        <ul class="fa-ul mb-0">
                            <li class="mb-2">
                                <span class="fa-li"><i class="fas fa-check"></i></span>
                                2020. 11. 12. 정보처리산업기사
                            </li>
                            <li class="mb-2">
                                <span class="fa-li"><i class="fas fa-check"></i></span>
                                2010. 07. 14. ITQ OA MASTER
                            </li>
                            <li class="mb-2">
                                <span class="fa-li"><i class="fas fa-check"></i></span>
                                2010. 04. 16. 워드프로세서 1급
                            </li>
                            <li class="mb-2">
                                <span class="fa-li"><i class="fas fa-check"></i></span>
                                2009. 05. 15. 컴퓨터활용능력 2급
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <hr class="m-0"/>
        <!-- Experience-->
        <section class="resume-section" id="experience">
            <div class="resume-section-content">
                <h2 class="mb-5">Experience</h2>
                <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
                    <div class="flex-grow-1">
                        <h3 class="mb-0">Link2US</h3>
                        <div class="subheading mb-3">개발팀 / 사원</div>
                        <ul class="fa-ul mb-0">
                            <li class="mb-2">
                                <span class="fa-li"><i class="fas fa-circle fa-xs"></i></span>
                                MES Lite 프로젝트 개발
                            </li>
                            <li class="mb-2">
                                <span class="fa-li"><i class="fas fa-circle fa-xs"></i></span>
                                Huvitz 프로젝트 개발 보조
                            </li>
                            <li class="mb-2">
                                <span class="fa-li"><i class="fas fa-circle fa-xs"></i></span>
                                고객사 서버 구성 및 설치 관리
                            </li>
                        </ul>
                        <br>
                        <div style="margin-left: 10px">
                            <p>
                                MES Lite 프로젝트의 기능 및 UI 개발을 맡아 진행했습니다.<br>
                                Java, JavaScript, jQuery 등의 언어를 주력으로 Front-end, Back-end, SQL 등의 코드를 작성하였고,
                                AXBOOT, Kendo UI 등 생소한 프레임워크도 개발사의 레퍼런스를 참고하며 개발하였습니다.
                            </p>
                        </div>
                    </div>
                    <div class="flex-shrink-0"><span class="text-primary">2019. 11. 01. - 2020. 04. 30.</span></div>
                </div>
            </div>
        </section>
        <hr class="m-0"/>
        <!-- Education-->
        <section class="resume-section" id="education">
            <div class="resume-section-content">
                <h2 class="mb-5">Education</h2>
                <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
                    <div class="flex-grow-1">
                        <h3 class="mb-0">공공데이터 기반 Java Application 개발자 과정</h3>
                        <div class="subheading mb-3">한국정보기술연구원</div>
                        <ul class="fa-ul mb-0">
                            <li class="mb-2">
                                <span class="fa-li"><i class="fas fa-circle fa-xs"></i></span>
                                웹 쇼핑몰 팀 프로젝트
                            </li>
                            <li class="mb-2">
                                <span class="fa-li"><i class="fas fa-circle fa-xs"></i></span>
                                AWS 연구 세미나
                            </li>
                        </ul>
                    </div>
                    <div class="flex-shrink-0"><span class="text-primary">2018. 05. 31. - 2018. 11. 19.</span></div>
                </div>
                <div class="d-flex flex-column flex-md-row justify-content-between">
                    <div class="flex-grow-1">
                        <h3 class="mb-0">현 장 실 습</h3>
                        <div class="subheading mb-3">㈜ 트리포드</div>
                        <ul class="fa-ul mb-0">
                            <li class="mb-2">
                                <span class="fa-li"><i class="fas fa-circle fa-xs"></i></span>
                                Windows Server 관리 실습
                            </li>
                            <li class="mb-2">
                                <span class="fa-li"><i class="fas fa-circle fa-xs"></i></span>
                                데이터 관리 교차 검증 실무
                            </li>
                            <li class="mb-2">
                                <span class="fa-li"><i class="fas fa-circle fa-xs"></i></span>
                                데이터 센터 견학
                            </li>
                        </ul>
                    </div>
                    <div class="flex-shrink-0"><span class="text-primary">2017. 07. 01. - 2017. 07. 31.</span></div>
                </div>
            </div>
        </section>
        <hr class="m-0"/>
        <!-- Admin Join -->
        <section class="resume-section" id="joinAdmin">
            <div class="resume-section-content">
                <h2 class="mb-5" style="text-align: center">Admin</h2>
                <div class="row" style="text-align: center;">
                    <button onclick="test();">테스트버튼이예용</button>
                </div>
            </div>
        </section>
        <hr class="m-0"/>

        <!-- Admin -->
        <section class="resume-section" id="admin" style="display: none">
            <div class="resume-section-content">
                <h2 class="mb-5" style="text-align: center">Admin </h2>
                <div class="row" style="text-align: center;">

                </div>
            </div>
        </section>
        <hr class="m-0"/>
    </div>
    </body>
</html>
